package worker.LoadBalancers;

import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.sync.RedisCommands;
import junit.framework.TestCase;
import redis.embedded.RedisServer;
import worker.Connections.MockReceiver;
import worker.Connections.RabbitmqReceiver;
import worker.Connections.Receiver;
import worker.DatabaseAccess.Lettuce;


import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


public class InputSimilarityTest extends TestCase {

    int cl = 2172;
    int c = 31006;
    String corredor = "Pirituba";
    String query2 = "SELECT o.p as tp, b.p as op, o.cl as cl,o.py as py, o.px as px,o.v as v, o.ta as ta, o.timestamp as timestamp from veli" + cl + "(p != " + c + ")#ext_timed(ta,30) as o, veli" + cl + "(p = " + c + ")#ext_timed(ta,30) as b WHERE distance(o.py, o.px, b.py, b.px) < 0.5 AND distance(o.py, o.px, b.py, b.px) > 0";
    String query3 = "SELECT cl, p, py, px, distance(py, px, prev(py), prev(px)) as d, distance(py, px, prev(py), prev(px))*1000*60*60/Math.abs(ta - prev(ta)) as v, ta, timestamp FROM bus" + cl + "#length(2) group by p ";
    String query4 = "SELECT cl, p, py, px, d, v, ta, timestamp FROM velf" + cl + " WHERE v > 0 AND v < 100";
    String query5 = "SELECT cl,p, py, px,ta,  avg(v) as speed, timestamp FROM veli" + cl + "#ext_timed(timestamp,300) group by p";
    String query6 = "Select cl, p, py, px,speed , avg(speed) as velocity, ta, timestamp FROM speedbus"+corredor+"#ext_timed(timestamp,300) HAVING speed < 10 and avg(speed) < 25";


    public void testfindEventTypetoRellocate() throws IOException {
        RedisServer redisServer = new RedisServer();
        redisServer.start();

        RedisClient redisClient = RedisClient.create("redis://localhost:6379/");
        StatefulRedisConnection<String, String> connection = redisClient.connect();
        RedisCommands<String,String> syncCommands = connection.sync();
        syncCommands.sadd("111:Inputs","001","002","003");
        syncCommands.set("111:Definition",query4);// no data window
        syncCommands.sadd("222:Inputs","001","002","004");
        syncCommands.set("222:Definition",query3);// lengh 2 data window
        syncCommands.sadd("333:Inputs","001","004");
        syncCommands.set("333:Definition",query5); // time window 300000
        connection.close();
        redisClient.shutdown();



        InputSimilarity lb = new InputSimilarity();

        Map<String, Receiver> receivers = new HashMap<>();

        receivers.put("001",new MockReceiver(100,10000));
        receivers.put("002",new MockReceiver(100000,100000));
        receivers.put("003",new MockReceiver(1000,1000));
        receivers.put("004",new MockReceiver(10,100000));


        /*

        HashSet<String> neoinputs = new HashSet<>();
        neoinputs.add("001");
        neoinputs.add("002");
        neoinputs.add("003");
        lb.updateRankplus("111",neoinputs,receivers);
        neoinputs = new HashSet<>();
        //neoinputs.remove("003");
        neoinputs.add("001");
        neoinputs.add("002");
        neoinputs.add("004");

        lb.updateRankplus("222",neoinputs,receivers);

        neoinputs = new HashSet<>();
        //neoinputs.remove("003");
        neoinputs.add("001");
        neoinputs.add("004");

        lb.updateRankplus("333",neoinputs,receivers);

         */

        Lettuce store = new Lettuce("localhost");

        lb.addEventTypeToRank("111",receivers,store);
        lb.addEventTypeToRank("222",receivers,store);
        lb.addEventTypeToRank("333",receivers,store);

        System.out.print("find first event111\n");
        assertEquals("111",lb.findEventTypetoRellocate());


        System.out.print("\ndelete 111\n");
        lb.deleteEventTypeofRank("111",receivers,store);

        System.out.print("\nfind second event 222\n");
        assertEquals("222",lb.findEventTypetoRellocate());

        store.Close();

        redisServer.stop();
    }

}
