package worker.ThreadHandler;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.avro.Schema;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import worker.ClusterHandler.ClusterHandler;
import worker.ClusterHandler.KubernetesHandler;
import worker.Connections.*;
import worker.DatabaseAccess.Lettuce;
import worker.Events.EventHandler;
import worker.Events.QueryAnalysis;
import worker.LoadBalancers.InputSimilarity;
//import worker.LoadBalancers.StateUsage;
import worker.LoadBalancers.LoadBalancer;
import worker.LoadBalancers.StateUsageTimeBased;
import worker.ResourcesAnalysis.ResourceAnalysis;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class ThreadHandler {
    private static final Logger LOG = LoggerFactory.getLogger(ThreadHandler.class);

    private static final String Broker = System.getenv("BROKER");

    private static final String LoadBalancingAlgorithm = System.getenv("LBA");

    private ClusterHandler workerHandler;

    private volatile EventHandler eventHandler;
    private double maxValue;
    //private ExecutorService executor;
    private final String WorkerId;
    private volatile LoadBalancer loadBalancer;

    private int kounters;

    private static final String exchange_name = System.getenv("EXCHANGE");

    private static final String rmode = System.getenv("RMODE");

    private static final boolean logs_th = Boolean.parseBoolean(System.getenv("LOGS_THREADHANDLER"));

    private static double resourceUsage = 0 ;

    private ConcurrentHashMap<String,Integer> numberOfinputTypeUses;


    private static final int  timeoutnotification = Integer.parseInt(System.getenv("TIMEOUT_NOT"));
    private static final int  timeoutRelocation = Integer.parseInt(System.getenv("TIMEOUT_REL"));
    private static final boolean logs_o = Boolean.parseBoolean(System.getenv("LOGS_OVERLOAD"));
    private static final boolean logs_u = Boolean.parseBoolean(System.getenv("LOGS_UNDERLOAD"));
    private static final boolean logs_i = Boolean.parseBoolean(System.getenv("LOGS_INITIATE"));
    private static final boolean logs_relo = Boolean.parseBoolean(System.getenv("LOGS_RELOCATION"));
    private static final boolean logs_deletion = Boolean.parseBoolean(System.getenv("LOGS_DELETION"));
    private static final boolean logs_add = Boolean.parseBoolean(System.getenv("LOGS_INCLUSION"));
    //private static boolean printRA;
    private ConcurrentHashMap<String, Receiver> ReceiverList;
    private ConcurrentHashMap<String, Sender> SenderList;

    private ConcurrentHashMap<String,Integer> counters;

    //private RabbitmqSender sender;

    private final DecimalFormat df = new DecimalFormat("#.##");

    private boolean hardOverload;
    private boolean softOverload;
    private boolean underload;
    private boolean middleload;
    //private boolean eventsExiting;
    private static final double softOverloadlimit = Double.parseDouble(System.getenv("SOFT_OVERLOAD"));
    private static final double hardOverloadlimit = Double.parseDouble(System.getenv("HARD_OVERLOAD"));
    private static final double middleloadlimit = softOverloadlimit+0.02;
    //private static final double firstgear = softOverloadlimit-0.05;
    private static final double underloadlimit = Double.parseDouble(System.getenv("UNDERLOAD"));

    private static final boolean logs_ra = Boolean.parseBoolean(System.getenv("LOGS_RESOURCEANALYSIS"));
    private boolean checkHardOverload = false;
    private boolean checkSoftOverload = false;
    //private final String RedisHost;

    private ResourceAnalysis RA;
    private Connection connR;
    private Connection connS;
    private Channel channelS;
    private Channel channelR;

    private Lettuce store1;
    private Lettuce store2;
    private Lettuce store3;


    public ThreadHandler(String WorkerID){
        LOG.debug("Creating Event Handler\n"); // Translate EPL statements into triggers
        this.eventHandler = new EventHandler();
        LOG.debug("Creating Event Sender\n "); // Class for sending events to RabbitMQ
        LOG.debug("Creating Event Receivers\n"); // Class for receiving events from other instances
        this.ReceiverList = new ConcurrentHashMap<>();
        this.SenderList = new ConcurrentHashMap<>();
        //this.eventsExiting = false;
        this.WorkerId = WorkerID;

        //this.RedisHost = redishost;

        this.store1 = new Lettuce(null);

        this.store2 = new Lettuce(null);
        this.store3 = new Lettuce(null);

        if(logs_th) System.out.println("TH | Max memory: "+(double) Runtime.getRuntime().maxMemory());

        this.numberOfinputTypeUses = new ConcurrentHashMap<>();

        if(logs_th) System.out.print("TH | Chosen Load Balancing Algorithm: "+LoadBalancingAlgorithm+"\n");
        if(("inputsimilarity").equals(LoadBalancingAlgorithm)) {
            this.loadBalancer = new InputSimilarity();
        }
        else if(("stateusage").equals(LoadBalancingAlgorithm)){
            this.loadBalancer = new StateUsageTimeBased();
        }
        //this.printRA = false;
        this.RA = new ResourceAnalysis();
        this.workerHandler = new KubernetesHandler();

        //Class for storing counters for rellocation purposes
        this.counters = new ConcurrentHashMap<>();

        if(logs_th) System.out.print("TH | Chosen Broker: "+Broker+"\n");
        if(("Rabbitmq").equals(Broker)&& (rmode.equals("conn")||rmode.equals("channel"))) {
            ConnectionFactory factory = new ConnectionFactory();
            factory.setHost(System.getenv("RABBITMQ_HOST"));
            factory.setUsername(System.getenv("RABBITMQ_USERNAME"));
            factory.setPassword(System.getenv("RABBITMQ_PASSWORD"));
            factory.setRequestedChannelMax(65535);
            try {
                connS = factory.newConnection();
                connR = factory.newConnection();
                if(logs_th) System.out.println("TH | Rabbitmq Connection in open ");
                if(logs_th) System.out.println("TH | Maximum number of channels: "+connR.getChannelMax());
                //if(logs_th) System.out.println("TH | Chaneel in open: "+channel.isOpen());
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }
            if(rmode.equals("channel")) {
                try {
                    channelS = connS.createChannel();
                    channelR = connR.createChannel();
                    channelR.exchangeDeclare(exchange_name, "topic");
                    channelS.exchangeDeclare(exchange_name, "topic");
                    //if(logs_th) System.out.println("TH | Chaneel in open: "+channel.isOpen());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    
    public void ContinuousSingleProcessing(){
        System.out.print("Mono | Single processing starting\n");
        //Lettuce store = new Lettuce(RedisHost);
        System.out.print("Mono | Begin Event Type Assignment\n");
        boolean status = true;
        while (status) {
            if (store1.hasUnnasignedEvents()) {
                List<String> EventTypeIds = store1.getUnnasignedEventTypes();
                System.out.print("mono | Adding event types in thread handler\n");
                for (String EventTypeId : EventTypeIds) {
                    System.out.print("Mono | Adding event typeId:"+EventTypeId+"\n");
                    AddOutgoingEventType(EventTypeId, store1);
                }
            }
            try {
                TimeUnit.MINUTES.sleep(15);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void InitiateAcceptionOfEvents(){
        if(logs_i )System.out.println("I  | Initiating accepting events on Worker "+WorkerId);
        //Lettuce store = new Lettuce(RedisHost);
        Set<String> lostEventTypes= store1.getAllEventTypesOfWorker(WorkerId);
        if(logs_i)System.out.println("I  | lost event types: "+lostEventTypes.toString());
        updateResource(store1,"Ini");
        if(lostEventTypes.isEmpty() && logs_i)
            System.out.println("I  | No events lost from previous session");
        else {
            store1.removeFullworker(WorkerId);
            if(logs_i) { System.out.println("I  | Retrivieng event types from previous session");}
            while (!lostEventTypes.isEmpty()) {
                for (String TypeId : lostEventTypes) {
                    if (!softOverload) {
                        AddOutgoingEventType(TypeId, store1);
                        lostEventTypes.remove(TypeId);
                        updateResource(store1,"Ini");
                    }
                }
                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    if(logs_i ) System.out.println("I  | Thread Failed to Sleep");
                    e.printStackTrace();
                }
            }
        }
        //store.Close();
        if(logs_i ) System.out.println("I  | Accepting event types");
        boolean status = true;
        String EventTypetoAcceptRealocation;

        int timeout_a = Integer.parseInt(System.getenv("TIMEOUT_A")) + (int)(Math.random()*100);
        int timeout_a2 = Integer.parseInt(System.getenv("TIMEOUT_A2")) + (int)(Math.random()*100);
        while (status) {

            //store = new Lettuce(RedisHost);
            updateResource(store1,"IniBase");
            if(!hardOverload&&!softOverload) {
                EventTypetoAcceptRealocation = store1.popEventTypeForAcceptingRellocation(softOverload,WorkerId);
                updateResourcesimple("IniAss");
                while (!softOverload && EventTypetoAcceptRealocation != null ) {
                    if (logs_i) System.out.println("I  | Accepting realocated event types");
                    if (logs_i)
                        System.out.println("I  | Accepting realocated event type " + EventTypetoAcceptRealocation);
                    //if(!store1.getEventTypeCurrentWorker(EventTypetoAcceptRealocation).equals(WorkerId))
                    AcceptRelocation(EventTypetoAcceptRealocation, store1);

                    updateResourcesimple("IniALoop");
                    if (logs_i) System.out.println("I  | event type " + EventTypetoAcceptRealocation + " Accepted");
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        if(logs_i ) System.out.println("I  | Thread Failed to Sleep");
                        e.printStackTrace();
                    }
                    EventTypetoAcceptRealocation = store1.popEventTypeForAcceptingRellocation(softOverload,WorkerId);
                    if (logs_i) {
                        System.out.println("I  |  next ET" + EventTypetoAcceptRealocation);
                        System.out.println("I  |  Soft Overload : " + softOverload);
                    }
                }

            }

            while (!middleload && store1.hasUnnasignedEvents()) {
                if (logs_i) System.out.println("I  | Accepting new event type .");
                AddUnnasignedEventType(store1);
                updateResourcesimpleM("Acceptint");
                if (softOverload) {
                    if (logs_i) System.out.println("I  | garbage collecting");
                    System.gc();
                }
                //updateResourceSupersimple(null);
            }


            if ((store1.getNumberOfFullWorkers() == store1.getTotalNumberOfWorkers()) && store1.hasUnnasignedEvents()) {
                if (logs_i) System.out.println("I  | This worker is full");

                if (logs_i) System.out.println("I  | full workers " + store1.getFullWorkers());
                //if(logs_o) System.out.println("I  | number of workers "+store.getTotalNumberOfWorkers());
                if (logs_i) System.out.println("I  | " + store1.getAllWorkers());
                if (logs_i) System.out.println("I  | All workers are full - starting new instance");
                workerHandler.instantiateNeoWorker(store1);
            }
            //store.Close();
            long timeout;
            if(softOverload)  timeout = timeout_a2;
            else timeout=timeout_a;

            try {
                Thread.sleep(timeout);
            } catch (InterruptedException e) {
                if (logs_i) System.out.println("I  | Thread Failed to Sleep");
                e.printStackTrace();
            }
        }
    }


    public void InitiateUnderloadMonitoring(){
        boolean status = true;
        //Lettuce store = new Lettuce(RedisHost);
        Set<String> deletedEventTypes;
        int tentatives = 0;
        String tentative;
        int timeout_u = Integer.parseInt(System.getenv("TIMEOUT_U"))+(int)(Math.random()*100);
        while (status) {
            try {
                if(logs_u ) System.out.println("U  | Timeout "+timeout_u/(1000)+" seconds");
                Thread.sleep(timeout_u);
            } catch (InterruptedException e) {
                if(logs_u ) System.out.println("U  | Thread failed to sleep");
                e.printStackTrace();
            }
            //store = new Lettuce(RedisHost);
            updateResource(store2,"Und");
            while (store2.hasUnnasignedEvents()){
                try {
                    if(logs_u ) System.out.println("U  | Waiting Unnasigned events");
                    Thread.sleep(60*1000);
                } catch (InterruptedException e) {
                    if(logs_u ) System.out.println("U  | Thread failed to sleep");
                    e.printStackTrace();
                }
            }
            deletedEventTypes = store2.getDeletedEventTypes(SenderList.keySet(),WorkerId);
            if(!deletedEventTypes.isEmpty()) {
                if(logs_u )System.out.println("U  | deleting old Event types checks "+deletedEventTypes);
                for (String deletedTypeId : deletedEventTypes) {
                    deleteCheckExpressionAndUnnecessaryInputs(store2, deletedTypeId);
                }
            }
            tentatives = 0;
            if(underload&&(store3.getNumberOfOverloadWorkers() < (store3.getTotalNumberOfWorkers()-store3.getNumberOfFullWorkers()))) {
                if (logs_u)
                    System.out.println("U  | Low resource usage: This worker is underloaded");
                String nextWorkerToRemovel;
                /*nextWorkerToRemovel = store2.getNextUnderloadedWorkerToRemoval();
                if(nextWorkerToRemovel == null) {
                    store2.setNextUnderloadedWorkerToRemoval(WorkerId);

                    try {
                        Thread.sleep(3000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }*/
                nextWorkerToRemovel = store2.getnextUnderloadWorkers();

                if(nextWorkerToRemovel.equals(WorkerId)) {
                    while (underload) {
                        if (numberOfinputTypeUses.isEmpty()&&ReceiverList.isEmpty()) {
                            workerHandler.StopAndRemoveWorkerFromCluster(WorkerId, store2);
                        }
                        String EventToRelocate = loadBalancer.findEventTypetoRellocate();
                        if (logs_u) System.out.println("U  | ET to Rellocate:" + EventToRelocate);
                        tentative = Relocate(RA.getResourceUsage(), EventToRelocate, store2,false);

                        if (tentative==null) {
                            tentatives++;
                            if (logs_u) System.out.println("U  | ET failed to realocate");
                        }
                        else{
                            if (logs_u) System.out.println("U  | This Worker"+WorkerId+" realocated ET "+EventToRelocate+" to Worker"+store2.getEventTypeCurrentWorker(EventToRelocate)+": " + tentative);
                        }
                        if (tentatives > 5) {
                            if (logs_u) System.out.println("U  | too much failed tentatives for realocation");
                            break;
                        }
                        updateResource(store2, "Und");
                    }
                }

            }
            //store.Close();
        }
    }
    static int timeout_o = Integer.parseInt(System.getenv("TIMEOUT_O"))+(int)(Math.random()*100);

    public void InitiateOverloadMonitoring(){
        boolean status = true;
        //Lettuce store = new Lettuce(RedisHost);
        //Set<String> deletedEventTypes;
        //String typeToRellocate;

        //boolean RealocationStarted;
        //boolean tentative;
        //int tentatives = 0;

        while (status) {
            try {
                Thread.sleep(timeout_o);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            updateResourcesimple(null);
            if(hardOverload) {
                //updateResource(store3,null);


                //printRA = true;
                //store = new Lettuce(RedisHost);
                if (logs_o) System.out.println("O  | High resurce usage: Realocating event types ");
                while (hardOverload) {
                    updateResource(store3,"OvI");
                    if((store3.getNumberOfOverloadWorkers()>(store3.getTotalNumberOfWorkers()-store3.getNumberOfFullWorkers()))){
                        if (logs_i) System.out.println("I  | full workers " + store3.getFullWorkers());
                        if (logs_i) System.out.println("I  | Number of Overloaded Workers is too large");
                        if (logs_i) System.out.println("I  | Instantiating new Worker");
                        createNeoWorker();
                    }
                    String EventToRelocate = loadBalancer.findEventTypetoRellocate();
                    if (logs_o) System.out.println("O  | ET to Rellocate:"+EventToRelocate);
                    //tentative =
                    Relocate(RA.getResourceUsage(),EventToRelocate, store3, true);
                    /*if (logs_o) System.out.println("O  | ET realocated: "+tentative);
                    if (!tentative) tentatives++;
                    if (tentatives > 25) {
                        //if (logs_o) System.out.println("O  | Failed to realocate to existing worker");
                        //createNeoWorker();
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            if (logs_o) System.out.println("O  | Thread failed to sleep");
                            e.printStackTrace();
                        }
                        tentatives = 0;
                    }

                     */
                    //updateResourcesimple("OvI2");
                }
                //store.Close();
            }
            //printRA = false;//-------------------------------------
        }
    }

    private void createNeoWorker(){
        int neoWorker = 0;
        if (logs_o) System.out.println("O  | Instatianting new Worker");
        while(neoWorker==0) {
            neoWorker = workerHandler.instantiateNeoWorker(store3);
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                if (logs_o) System.out.println("O  | Thread failed to sleep");
                e.printStackTrace();
            }
        }


        if (logs_o) System.out.println("O  | New Worker Id is "+neoWorker);
        while(!store3.getActiveWorkers().contains(String.valueOf(neoWorker))){
            //System.out.println("Current Full workers : "+store3.getFullWorkers());
            //System.out.println("Current Active workers : "+store3.getAllWorkers());

            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                if (logs_o) System.out.println("O  | Thread failed to sleep");
                e.printStackTrace();
            }
        }
        if (logs_o) System.out.println("O  | Worker"+neoWorker+" is running");

    }

    private void AcceptRelocation(String OutputTypeID, Lettuce store){

        if(!softOverload) {
            if (logs_relo) System.out.println("AR | 0 -Events not exiting, get sending worker ");

            String SendingWorkerID = setUpReceivingAndSendingWorkerIds(store, OutputTypeID);
            if (logs_relo) System.out.print("AR | 1 - sendingWorkerId: " + SendingWorkerID);
            if (logs_relo) System.out.println(" - Adding Event type "+OutputTypeID+" to runtime: ");
            createStoppedSender(OutputTypeID);
            addEventTypeToRuntime(store, OutputTypeID);
            if (logs_relo) System.out.println("AR | 2 - Evaluating time to wait ");

            //Evaluating time and events needed to build state
            long timeToWait = evaluateInputsToBuildState(store, OutputTypeID);
            if (logs_relo)
                System.out.print("AR | 3 - Time to wait for realocation of Event type "+OutputTypeID +" is " + timeToWait + " microseconds");

            //building state
            if (logs_relo) System.out.println(" - Building State ");
            buildStateAndSave(store, OutputTypeID, timeToWait);


            if (logs_relo)
                System.out.println("AR | 4 - This Worker"+WorkerId+" is Waiting for worker "+SendingWorkerID+" to finish processing of Event Type " + OutputTypeID);
            //waiting for other worker to stop sending events
            waitSendingWorkerNotification(OutputTypeID, SendingWorkerID, store);


            //reset current worker in db
            if (logs_relo) System.out.println("AR | 5 - Start Sending Event type " + OutputTypeID);
            startSendingEvents(OutputTypeID);

            if (logs_relo) System.out.println("AR | 6 - closing realocation process ");
            closeEventRealocation(store,OutputTypeID);


            //update load balancer rank
            if (logs_relo)
                System.out.println("AR | 7 - Update Load Balancer with Event type " + OutputTypeID);
            loadBalancer.addEventTypeToRank(OutputTypeID, ReceiverList,store);
            if (logs_relo) System.out.println("AR | 8 - finished acception of event "+OutputTypeID);
        }
    }

    private String setUpReceivingAndSendingWorkerIds(Lettuce store, String OutputTypeID){
        store.setEventTypeReceveingWorker(OutputTypeID,WorkerId);
        return store.getEventTypeCurrentWorker(OutputTypeID);
    }

    private void addEventTypeToRuntime(Lettuce store, String OutputTypeID){
        String Name = store.getEventTypeName(OutputTypeID);
        String Query = store.getEventTypeDefinition(OutputTypeID);
        Set<String> inputs = store.getEventTypeInputs(OutputTypeID);
        if(logs_add)System.out.println("ADD | Inputs for EventId "+OutputTypeID+" : "+ inputs.toString());
        CreateNewInputs(inputs,store,OutputTypeID);
        if(logs_add ) System.out.println("ADD | Setting checking expression for "+OutputTypeID);
        eventHandler.addCheckExpression(OutputTypeID,Name, Query, SenderList.get(OutputTypeID));
        if(logs_add ) System.out.print("ADD | Including Event Type Id : "+OutputTypeID+" on EventHandler\n");
        store.setAvroSchema(OutputTypeID,eventHandler.getSchema(OutputTypeID));
    }

    private void CreateNewInputs(Set<String> neoinputs, Lettuce store, String OutputTypeId){
        for(String neoInput : neoinputs){
            if(!numberOfinputTypeUses.containsKey(neoInput)){
                numberOfinputTypeUses.put(neoInput,1);
                eventHandler.addInputStream(store.getEventTypeName(neoInput),store.getAvroSchema(neoInput));
            }
            numberOfinputTypeUses.put(neoInput,numberOfinputTypeUses.get(neoInput)+1);

            if(!ReceiverList.containsKey(neoInput)){
                if(logs_add ) System.out.println("TH | Creating new receiver for "+neoInput);
                ReceiverList.put(neoInput,NewReceiver(store.getEventTypeName(neoInput), store.getAvroSchema(neoInput),neoInput,eventHandler));
            }
        }

    }

    private long evaluateInputsToBuildState(Lettuce store,String OutputTypeID){
        Set<String> neoIncomingIds;
        String Query = store.getEventTypeDefinition(OutputTypeID);
        long timeToWait = 0;
        neoIncomingIds = store.getEventTypeInputs(OutputTypeID);
        String name;
        String stype;
        String snumber;
        kounters = 0;
        //if (logs_relo) System.out.println("AR | calculating time to wait ");
        for(String id : neoIncomingIds){
            //if (logs_relo) System.out.println("AR | new Id "+id);
            name = store.getEventTypeName(id);
            if(QueryAnalysis.hasDataWindow(Query)) {
                //if (logs_relo) System.out.println("AR | has data window ");
                stype = QueryAnalysis.getStateType(Query);
                snumber = QueryAnalysis.getStateNumber(Query);
                //if (logs_relo) System.out.println("AR | State type (" + stype+"), and number ("+snumber+")");
                if(QueryAnalysis.IsItTimeBased(Query)&&timeToWait<QueryAnalysis.analyzeQueryTime(stype, snumber)) {
                    timeToWait = QueryAnalysis.analyzeQueryTime(stype, snumber);
                    //if (logs_relo) System.out.println("AR | Time To Wait (" + timeToWait + ")");
                }
                else if(kounters<QueryAnalysis.analyzeQueryLengh(stype, snumber)) {
                    kounters = QueryAnalysis.analyzeQueryLengh(stype, snumber);
                    //if (logs_relo) System.out.println("AR |  and counters (" + counters + ")");
                }
                //if (logs_relo) System.out.println("AR | State type (" + stype+"), and number ("+snumber+")");
                //only accept value in seconds ( e.g.(3))
            }
        }
        return timeToWait;
    }

    private void buildStateAndSave(Lettuce store, String OutputTypeID,long timeToWait){
        //time to wait in microsseconds
        //createStoppedSender(OutputTypeID);
        Sender sender = SenderList.get(OutputTypeID);
        boolean countersachieved = false;
        while(!countersachieved){ //waiting for state to build
            if (logs_relo) System.out.println("AR | counters needed " + kounters);
            if (logs_relo) System.out.println("AR | counters left " + sender.getEventsLost());

            if (logs_relo) System.out.println("AR | time to wait " + timeToWait);
            try {
                Thread.sleep(timeToWait+100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if((sender.getEventsLost()>=kounters))
                countersachieved = true;
        }
        store.eventTypeStateBuildedAdd(OutputTypeID,WorkerId);
        kounters = 0;
        counters.remove(OutputTypeID);
    }

    private void createStoppedSender(String OutputTypeID){
        Sender sender = NewSender();
        sender.stopSending();
        SenderList.put(OutputTypeID,sender);
    }

    private void waitSendingWorkerNotification(String OutputTypeID,String SendingWorkerID,Lettuce store){
        boolean transferConfirmed = false;
        while(!transferConfirmed){
            if(!store.EventTypeCurrentWorker(OutputTypeID,SendingWorkerID))
                transferConfirmed = true;
            try {
                Thread.sleep(100); //10000
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //store.removeEventTypeReceveingWorker(OutputTypeID);
    }
    private void startSendingEvents(String OutputTypeID){
        SenderList.get(OutputTypeID).restartSending();
    }

    private void closeEventRealocation(Lettuce store,String OutputTypeID){
        store.removeEventTypeReceveingWorker(WorkerId);
        store.RemoveEventTypeForRellocation(OutputTypeID);
    }

    private String Relocate(double resourceusage, String EventTypeId, Lettuce store, boolean logs ){

        if(logs_relo &&logs)System.out.println("Realocation | 0 - Pushing Event type "+EventTypeId+" to Relocation to other worker");
        //Lettuce store = new Lettuce(RedisHost);

        putEventForRelocation(store,resourceusage,EventTypeId);

        if(logs_relo &&logs)System.out.println("Realocation | 1 - Waiting for any other worker to pickup Event type "+EventTypeId+" for realocation");
        waitForEventToBePickUpForRelocation();


        if(eventHasBeenPickedUpForRelocation(store,EventTypeId)&&!store.getEventTypeReceiveingWorker(EventTypeId).equals(WorkerId)) {

            if (logs_relo&&logs) System.out.println("Realocation | 2 - Evaluating time to wait ");
            long timeToWait = evaluateInputsToBuildState(store,EventTypeId);

            if (logs_relo&&logs)
                System.out.println("Realocation | 3 - This Worker"+WorkerId+" is Waiting worker"+store.getEventTypeReceiveingWorker(EventTypeId)+" to build state");
            waitForStateToBeRebuildedElseware(store, EventTypeId,timeToWait);

            if (logs_relo&&logs)
                System.out.println("Realocation | 4 - Acknoledge tranfer of Event type " + EventTypeId + " from worker"+WorkerId+" to worker"+store.getEventTypeReceiveingWorker(EventTypeId));
            acknolegeTransferConfirmed(store, EventTypeId);

            if (logs_relo&&logs)
                System.out.println("Realocation | 5 - Remove Event type " + EventTypeId + " check expression and inputs");
            deleteCheckExpressionAndUnnecessaryInputs(store, EventTypeId);


            //update load balancer rank
            if (logs_relo&&logs) System.out.println("Realocation | 6 - Remove Event type " + EventTypeId + " from load balancer");
            loadBalancer.deleteEventTypeofRank(EventTypeId, ReceiverList,store);

            if (logs_relo&&logs) System.out.println("Realocation | 7 - finished realocation of event "+EventTypeId);
            //eventsExiting=false;
            return store.getEventTypeCurrentWorker(EventTypeId);
        }
        else {
            if(logs_relo &&logs)System.out.println("Realocation | -2 - Event type "+EventTypeId+" failed to be realocated");
            undoPutEventForRelocation(store,EventTypeId);
            //eventsExiting=false;
            return null;
        }
    }

    private void putEventForRelocation(Lettuce lettuce, double resourceusage, String EventTypeId){
        lettuce.pushEventTypeForRellocation(EventTypeId, resourceusage);
    }

    private void undoPutEventForRelocation(Lettuce lettuce, String EventTypeId){
        lettuce.RemoveEventTypeForRellocation(EventTypeId);
    }
    private void waitForEventToBePickUpForRelocation(){
        //if(logs_relo )System.out.println("Realocation | Waiting "+timeoutRelocation/1000+" seconds");
        try {
            Thread.sleep(timeoutRelocation);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
    private boolean eventHasBeenPickedUpForRelocation(Lettuce lettuce, String EventTypeId){
        return lettuce.hasEventTypeBeenPickedUpForRellocation(EventTypeId,WorkerId);
    }
    private void waitForStateToBeRebuildedElseware(Lettuce store, String EventTypeId,long timeToWait){
        String ReceveingWorkerID = store.getEventTypeReceiveingWorker(EventTypeId);
        boolean stateRebuiledElsewhere = store.eventTypeStateBuilded(EventTypeId,ReceveingWorkerID);
        while(!stateRebuiledElsewhere) {
            try {
                //time to wait comes in microsseconds
                Thread.sleep(timeToWait);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.gc();
            stateRebuiledElsewhere = store.eventTypeStateBuilded(EventTypeId,ReceveingWorkerID);
        }
    }
    private void acknolegeTransferConfirmed(Lettuce store, String OutputTypeID){
        String newWorker = store.getEventTypeReceiveingWorker(OutputTypeID);
        store.setEventTypeCurrentWorker(OutputTypeID,newWorker);
        //store.removeEventTypeReceveingWorker(OutputTypeID);
    }
    private void deleteCheckExpressionAndUnnecessaryInputs(Lettuce store,String EventTypeId){

        eventHandler.deleteCheckExpression(EventTypeId);
        Set<String> inputs = store.getEventTypeInputs(EventTypeId);
        RemoveUnnecessaryInputs(inputs,EventTypeId);
        //remEventTypeToRuntime(store,EventTypeId);

        if(logs_deletion) System.out.println("D  | delete Sender for "+EventTypeId);
        DeleteSender(SenderList.get(EventTypeId));
        if(logs_deletion) System.out.println("D  | remove Sender ");
        SenderList.remove(EventTypeId);
        if(logs_deletion) System.out.println("D  |  "+EventTypeId+" completely removed");


    }

    private void RemoveUnnecessaryInputs(Set<String> oldinputs,String OutputTypeId){
        Set<String> UsedTypeNames = new HashSet<>();
        for(String Input : oldinputs){
            if(numberOfinputTypeUses.containsKey(Input)){
                if(numberOfinputTypeUses.get(Input)>1){
                    numberOfinputTypeUses.put(Input,numberOfinputTypeUses.get(Input)-1);
                }
                else{
                    numberOfinputTypeUses.remove(Input);
                    DeleteReceiver(ReceiverList.get(Input));
                    ReceiverList.remove(Input);
                    UsedTypeNames.add(Input);
                }
            }
            /*else{
                if(ReceiverList.containsKey(Input)) {
                    DeleteReceiver(ReceiverList.get(Input));
                    ReceiverList.remove(Input);
                    UsedTypeNames.add(Input);
                }
            }*/
        }
        eventHandler.removeInputs(UsedTypeNames);
    }

    //-----------------------------------------------------------------------------------------------------------


    //Adding Event Types


    private void AddUnnasignedEventType(Lettuce store){
        String EventTypeId = store.getUnnasignedEventType();
        store.eventTypeAssigned(EventTypeId);
        if(logs_add ) System.out.print("TH | Storing Event Type Id : "+EventTypeId+" as assigned\n");
        AddOutgoingEventType(EventTypeId,store);
    }

    private void AddOutgoingEventType(String OutputTypeID,Lettuce store){
        if(logs_add)System.out.println("TH | Adding Outgoing EventId : "+OutputTypeID);

        createStoppedSender(OutputTypeID);
        addEventTypeToRuntime(store,OutputTypeID);
        startSendingEvents(OutputTypeID);

        if(logs_add ) System.out.print("TH | Storing Event Type Id : "+OutputTypeID+" current Worker\n");
        store.setEventTypeCurrentWorker(OutputTypeID,WorkerId);

        //inputs.clear();
        //update load balancer rank
        if(logs_add ) System.out.print("TH | Including  "+OutputTypeID+" on the load balancer\n");
        loadBalancer.addEventTypeToRank(OutputTypeID,ReceiverList,store);
        if(logs_add ) System.out.print("TH | Event Type "+ store.getEventTypeName(OutputTypeID) +" is being detected\n");
    }



    // Manage Senders and Receivers

    private Sender NewSender(){
        if(Broker.equals("Rabbitmq")){
            if(rmode.equals("conn"))  return new RabbitmqSender(connS);
            else if(rmode.equals("channel"))  return new RabbitmqSender(channelS);
            else return new RabbitmqSender();
        }
        else return new NATSSender();
    }

    private void DeleteSender(Sender sender){ sender.close(rmode); }

    private Receiver NewReceiver(String TypeName, Schema schema, String TypeId, EventHandler eventHandler){
        if(Broker.equals("Rabbitmq")){
            if(rmode.equals("channel")) return new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler,channelR);
            else if(rmode.equals("conn")) return new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler,connR);
            else return new RabbitmqReceiver(schema,TypeId,TypeName,eventHandler);

        }
        else return new NATSReceiver(TypeName,schema,TypeId,eventHandler);
    }

    private void DeleteReceiver(Receiver receiver){ receiver.close(rmode); }

    // Update boolean resource variables

    private void updateResource(Lettuce store,String p){
        maxValue = RA.getResourceUsage();
        if (maxValue > softOverloadlimit) {
            softOverload = true;
            store.setFullWorker(WorkerId);
        }
        else{
            softOverload = false;
            store.remFullWorker(WorkerId);
        }
        //hardOverload = maxValue > hardOverloadlimit;
        if (maxValue > hardOverloadlimit) {
            hardOverload = true;
            store.setOverloadWorker(WorkerId);
        }
        else{
            hardOverload = false;
            store.remOverloadWorker(WorkerId);
        }
        underload = maxValue < underloadlimit && maxValue < underloadlimit;
        if (underload) {
            store.setUnderloadWorker(WorkerId,maxValue);
        }
        else{
            store.remUnderloadWorker(WorkerId);
        }
        middleload = maxValue > middleloadlimit;
        //eventsExiting = (underload&&!store.hasUnnasignedEvents() )|| hardOverload;
        if(logs_ra&&p!=null) System.out.println("RA | "+p+" Resources usage: "+df.format(maxValue));
    }

    private void updateResourcesimple(String p){
        maxValue = RA.getResourceUsage();
        softOverload = maxValue > softOverloadlimit;
        hardOverload = maxValue > hardOverloadlimit;
        underload = maxValue < underloadlimit && maxValue < underloadlimit;
        middleload = maxValue > middleloadlimit;
        //System.out.println("middle OVERLOAD limit: "+middleloadlimit);
        if(logs_ra&&p!=null) System.out.println("RA | "+p+" Resources usage: "+df.format(maxValue));
    }

    private void updateResourcesimpleM(String p){
        maxValue = RA.getResourceUsage();
        softOverload = maxValue > softOverloadlimit;
        hardOverload = maxValue > hardOverloadlimit;
        underload = maxValue < underloadlimit && maxValue < underloadlimit;
        middleload = maxValue > middleloadlimit;
        if(logs_ra) System.out.print("RA | mdld: "+middleload);
        if(logs_ra) System.out.println(" Acc R. usage: "+df.format(maxValue));
    }
}
