package worker.ResourcesAnalysis;
import javax.management.*;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;

public class ResourceAnalysis {

    //private static boolean logs_ra = Boolean.parseBoolean(System.getenv("LOGS_RESOURCEANALYSIS"));
    private static final double max_pod_b = Double.parseDouble(System.getenv("JAVA_MEM"))*1048576;
    //private static final double java_mem = Double.parseDouble(System.getenv("JAVA_MEM"));
    private MBeanServer mbs;
    private MemoryMXBean memBean;
    //private static final DecimalFormat df = new DecimalFormat("####.##");
    //private DecimalFormat dfa = new DecimalFormat("#");

    private double hm;
    private double nhm;
    private Attribute att;
    private double percentage = Double.NaN;
    private ObjectName name = null;
    private AttributeList list = null;
    private Double value = Double.NaN;


    public ResourceAnalysis(){
        mbs = ManagementFactory.getPlatformMBeanServer();
        memBean = ManagementFactory.getMemoryMXBean();
        //dfa.setMaximumFractionDigits(4);
    }


    private double getProcessCpuLoad() {
        while(percentage == Double.NaN) {
            //MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
            try {
                name = ObjectName.getInstance("java.lang:type=OperatingSystem");
                list = mbs.getAttributes(name, new String[]{"ProcessCpuLoad"});
            } catch (MalformedObjectNameException | InstanceNotFoundException | ReflectionException e) {
                e.printStackTrace();
            }

            if (!list.isEmpty()) {
                att = (Attribute) list.get(0);
                value = (Double) att.getValue();

                // usually takes a couple of seconds before we get real values
                if (value != -1.0) {
                    percentage = value;
                }
            }
            // returns a percentage value with 1 decimal point precision
        }
        //if(logs_ra) System.out.println("RA | CPU LOAD : "+((int) (percentage * 1000) / 10.0)+" from 0 to 1");
        return ((int) (percentage * 1000) / 10.0);
    }

    private double getMemoryUsage(){
        //double totalMemory = (double) Runtime.getRuntime().totalMemory();  // in MB
        //double usedMemory = totalMemory - Runtime.getRuntime().freeMemory();
        //double usedPercent;
       // double maxMemory;
        //maxMemory = (double) Runtime.getRuntime().maxMemory();

        hm = ((double) (memBean.getHeapMemoryUsage().getUsed()))/max_pod_b;
        nhm = ((double) (memBean.getNonHeapMemoryUsage().getUsed()))/max_pod_b;

        //usedPercent = usedMemory / maxMemory;
        /*
        if (logs_ra) {
            //System.out.println("RA | MEMORY LOAD : " + df.format(usedPercent));
            // "RA | HEAP%: "+df.format(hm)+", \nNON HEAP%:  " + df.format(nhm)+", \n
            System.out.println("RA | TOTAL%:     "+df.format(hm + nhm)+" , "+df.format(((hm + nhm)*java_mem)));
            //System.out.print();
            //System.out.print(", TOTAL%:"+df.format(hm + nhm));
            //System.out.println(", TOTAL:"+df.format(((hm + nhm)*java_mem)));
        }

         */
        return (hm + nhm);
    }

    public double getResourceUsage(){
        return Math.max(getMemoryUsage(), getProcessCpuLoad());
    }

    /*
    public static boolean softOverload(){
        boolean cpuoverload = getProcessCpuLoad() > Double.parseDouble(System.getenv("SOFT_OVERLOAD"));
        boolean memoryoverload = getMemoryUsage() > Double.parseDouble(System.getenv("SOFT_OVERLOAD"));
        return cpuoverload || memoryoverload;
    }



    public static boolean hardOverload(){
        boolean cpuoverload = getProcessCpuLoad() > Double.parseDouble(System.getenv("HARD_OVERLOAD"));
        boolean memoryoverload = getMemoryUsage() > Double.parseDouble(System.getenv("HARD_OVERLOAD"));
        return cpuoverload || memoryoverload;
    }

    public static boolean Underload(){
        boolean cpuUnderload = getProcessCpuLoad() < Double.parseDouble(System.getenv("UNDERLOAD"));
        boolean memoryUnderload = getMemoryUsage() < Double.parseDouble(System.getenv("UNDERLOAD"));
        return cpuUnderload && memoryUnderload;
    }

     */


}
