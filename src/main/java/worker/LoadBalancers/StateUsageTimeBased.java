package worker.LoadBalancers;


import worker.Connections.Receiver;
import worker.DatabaseAccess.Lettuce;
import worker.Events.QueryAnalysis;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.ConcurrentSkipListSet;

public class StateUsageTimeBased implements LoadBalancer {

    private final ConcurrentSkipListMap<String,Double> TimeToBuildMap;
    //private ConcurrentSkipListMap<String,String> ETDefinitions;
    //private ConcurrentSkipListMap<String,Set<String>> ETInputs;
    private static boolean logs_lba = Boolean.parseBoolean(System.getenv("LOGS_LBA"));
    //private final String host;
    //private final Integer WorkerId;
    //ConcurrentSkipListSet<ScoredStringD> TimeRanked;
    //private ConcurrentSkipListSet<ScoredStringD> CountersTimeRanked;
    private int slot;
    private final ConcurrentSkipListMap<Double,ConcurrentSkipListSet<String>> RankedTime;
    private ConcurrentSkipListMap<Double,ConcurrentSkipListSet<String>> RankedCounters;




    public StateUsageTimeBased(){
        //host = hostname;
        //this.WorkerId = WorkerId;
        TimeToBuildMap = new ConcurrentSkipListMap<>();
        //CountersTimeRanked = new ConcurrentSkipListSet<>();
        slot = 0;
        //ETDefinitions = new ConcurrentSkipListMap<>();
        //TimeRanked = new ConcurrentSkipListSet<>();
        //ETInputs = new ConcurrentSkipListMap<>();
        RankedTime = new ConcurrentSkipListMap<>();
        RankedCounters = new ConcurrentSkipListMap<>();
    }

    public StateUsageTimeBased(boolean logs){
        //host = hostname;
        //this.WorkerId = WorkerId;
        TimeToBuildMap = new ConcurrentSkipListMap<>();
        //CountersTimeRanked = new ConcurrentSkipListSet<>();
        slot = 0;
        //ETDefinitions = new ConcurrentSkipListMap<>();
        //TimeRanked = new ConcurrentSkipListSet<>();
        //ETInputs = new ConcurrentSkipListMap<>();
        RankedTime = new ConcurrentSkipListMap<>();
        logs_lba = logs;
        RankedCounters = new ConcurrentSkipListMap<>();
    }

    // Fazer ranking de tempo, usando o fluxo como base pra calculo.


    public String findEventTypetoRellocate(){
        if(logs_lba) {
            //System.out.println("\nfind Event");
            printstatus();}
        if(RankedTime.size()==0&&RankedCounters.size()==0)
            return null;
        else if(RankedTime.size()==0){
            return RankedCounters.firstEntry().getValue().first();
        }
        else if(RankedCounters.size()==0){
            return RankedTime.firstEntry().getValue().first();
        }
        else if(RankedTime.firstEntry().getKey()<=RankedCounters.firstEntry().getKey()){
            return RankedTime.firstEntry().getValue().first();
        }
        else return RankedCounters.firstEntry().getValue().first();
    }




    /*public String findEventTypetoRellocate2() {
        if(logs_lba) printstatus();
        if(TimeRanked.size()==0&&CountersTimeRanked.size()==0){
            //if(logs_lba) System.out.println("LBA-SU |All ranks are empty");
            return null;
        }
        else if(TimeRanked.size()==0) {
            //if (logs_lba) System.out.print("LBA-SU |TimeRank is Empty and CounterRank has" + CountersTimeRanked.size());
            //if (logs_lba) System.out.println("  Returning uuid"+CountersTimeRanked.first().getString()+" with score"+CountersTimeRanked.first().getScore());
            return CountersTimeRanked.first().getString();

        }
        else if(CountersTimeRanked.size()==0) {
            //if (logs_lba) System.out.print("LBA-SU |CounterRank is Empty and TimeRank has" + TimeRanked.size());
            //if (logs_lba) System.out.println("  Returning uuid"+TimeRanked.first().getString()+" with score"+TimeRanked.first().getScore());
            return TimeRanked.first().getString();
        }
        else if(TimeRanked.first().getScore()<=CountersTimeRanked.first().getScore()){
            //if (logs_lba) System.out.print("LBA-SU |TimeRank has "+TimeRanked.size()+" ,CounterRank has" + CountersTimeRanked.size());
            //if (logs_lba) System.out.println("  Returning uuid"+TimeRanked.first().getString()+" with score"+TimeRanked.first().getScore());
            return TimeRanked.first().getString();
        }
        else {
            //if (logs_lba) System.out.print("LBA-SU |TimeRank has "+TimeRanked.size()+" ,CounterRank has" + CountersTimeRanked.size());
            //if (logs_lba) System.out.println("  Returning uuid" + CountersTimeRanked.first().getString() + " with score" + CountersTimeRanked.first().getScore());
            return CountersTimeRanked.first().getString();
        }
    }*/

    private void printstatus(){
        System.out.println("LBA-SU | number of ETS in LB: "+size());
        System.out.println("\nRankedTime:");
        if(RankedTime.size()==0) System.out.println(" RankedTime are empty");
        else {
            for (double scores : RankedTime.keySet()) {
                System.out.println("Score : "+scores);
                for(String d : RankedTime.get(scores)){
                    System.out.print(d+" , ");
                }
            }
        }
        System.out.println("\nRankedCounters:");
        if(RankedCounters.size()==0) System.out.println("\nRankedCounters are empty");
        else {
            for (double scores : RankedCounters.keySet()) {
                System.out.println("Score : "+scores);
                Set<String> what = RankedCounters.get(scores);
                System.out.println("Score size : "+what.size());
                System.out.println("Score inside : "+what.toString());
                for(String d : what){
                    System.out.print("d is "+d+" , ");
                }
            }
            //System.out.println("key set of RankedCounters"+RankedCounters.keySet());
            //System.out.println("key values of RankedCounters"+RankedCounters.values());
            //for (double scores : RankedCounters.keySet()) {
            //    System.out.println("Score "+scores+" has value "+RankedCounters.get(scores).toString());
           // }



        }

        /*if(TimeRanked.size()==0) System.out.println(" Timerank are empty");
        else {
            for (ScoredStringD d : TimeRanked) {
                System.out.println("Id : " + d.getString() + " , score : " + d.getScore());
            }
        }
        System.out.println("\nCountersTimeRank:");
        if(CountersTimeRanked.size()==0) System.out.println("LBA-SU | CountersTimerank are empty");
        else {
            for (ScoredStringD d : CountersTimeRanked) {
                System.out.println("Id : " + d.getString() + " , score : " + d.getScore());
            }
        }

         */
    }

    public int size(){
        //return TimeRanked.size()+CountersTimeRanked.size();
        return RankedTime.size()+RankedCounters.size();
    }

    public ConcurrentSkipListMap<Double, ConcurrentSkipListSet<String>> allETs(){
        ConcurrentSkipListMap<Double, ConcurrentSkipListSet<String>> result = new ConcurrentSkipListMap<>();
        for(double d : RankedTime.keySet()){
            result.put(d,RankedTime.get(d));
        }
        for(double d : RankedCounters.keySet()){
            result.put(d,RankedCounters.get(d));
        }
        //ConcurrentSkipListMap<Double, ConcurrentSkipListSet<String>> result2 = RankedCounters.clone();
        //result.merge(key, value, (v1, v2) -> new Employee(v1.getId(),v2.getName())
        return result;
    }



    public void addEventTypeToRank(String uuid, Map<String, Receiver> receiverMap,Lettuce store){

        String definition = store.getEventTypeDefinition(uuid);
        //System.out.println(" TypeId");
        //ETDefinitions.put(uuid,definition);
        if(!QueryAnalysis.hasDataWindow(definition)){
            //TimeRanked.add(new ScoredStringD(uuid,0.0));
            if(RankedTime.get(0.0)==null){
                RankedTime.put(0.0,new ConcurrentSkipListSet<>());
            }
            RankedTime.get(0.0).add(uuid);

        }
        else {
            if (QueryAnalysis.IsItTimeBased(definition)) {
                String stype = QueryAnalysis.getStateType(definition);
                String snumber = QueryAnalysis.getStateNumber(definition);
                double querytime = (double) QueryAnalysis.analyzeQueryTime(stype, snumber);
                //TimeRanked.add(new ScoredStringD(uuid, querytime));
                if(RankedTime.get(querytime)==null){
                    RankedTime.put(querytime,new ConcurrentSkipListSet<>());
                }
                RankedTime.get(querytime).add(uuid);

            } else {
                //ETInputs.put(uuid,store.getEventTypeInputs(uuid));
                TimeToBuildMap.put(uuid,(double) (24*60*60*1000));
                updateRank(receiverMap,store);
                if(logs_lba){
                    System.out.print("TimetoBuildMap updated size :"+TimeToBuildMap.size());

                }
            }
        }
        //System.out.println("\nAdd Event");
        //printstatus();
        //System.out.println("\nTimeToBuildMap events "+TimeToBuildMap.keySet());
    }


    public void deleteEventTypeofRank(String uuid, Map<String, Receiver> receiverMap,Lettuce store){

        //String definition = store.getEventTypeDefinition(uuid);
        String definition = store.getEventTypeDefinition(uuid);
        if(!QueryAnalysis.hasDataWindow(definition)){
            //TimeRanked.remove(new ScoredStringD(uuid,0.0));
            if(RankedTime.get(0.0).size()>1)
                RankedTime.get(0.0).remove(uuid);
            else
                RankedTime.remove(0.0);
        }
        else {
            if (QueryAnalysis.IsItTimeBased(definition)) {
                String stype = QueryAnalysis.getStateType(definition);
                String snumber = QueryAnalysis.getStateNumber(definition);
                double querytime = (double) QueryAnalysis.analyzeQueryTime(stype, snumber);
                //TimeRanked.remove(new ScoredStringD(uuid, QueryAnalysis.analyzeQueryTime(stype, snumber)));
                RankedTime.get(querytime).remove(uuid);

            } else {
                TimeToBuildMap.remove(uuid);
                //ETInputs.remove(uuid);
                updateRank(receiverMap,store);
            }
        }
        //ETDefinitions.remove(uuid);
    }

    private void updateRank(Map<String, Receiver> receiverMap, Lettuce store){
        slot++;
        slot = slot%5;
        Map<String,Double> frequencyOfinputevents = getFrequencyOfInputEventsPassed(receiverMap,store);

        //for(ScoredStringL ss : CountersTimeRanked){ TimeRanked.remove(ss); }
        //CountersTimeRanked.clear();
        RankedCounters =  new ConcurrentSkipListMap<>();

        OrderTypesByInputEvents(frequencyOfinputevents,store);
        //System.out.println("TimeToBuildMap events "+TimeToBuildMap.keySet());
        for(String key : TimeToBuildMap.keySet()){
            double avgTimeElapsed = TimeToBuildMap.get(key);


            /*int j = 5;
            for(int i = 0; i <5 ; i++){
                if(TimeToBuildMap.get(key)[i]!=null) {
                    avgTimeElapsed += TimeToBuildMap.get(key)[i];
                }
                if(TimeToBuildMap.get(key)[i]==null) j--;
                System.out.println("indice "+i+" de "+key+" tem "+TimeToBuildMap.get(key)[i]);
            }
            System.out.println(" j is "+j);
            avgTimeElapsed = avgTimeElapsed/(5-j);

             */

            //ScoredStringD noe = new ScoredStringD(key,avgTimeElapsed);
            //TimeRanked.add(noe);
            //System.out.println("AvgTimeElapsed is "+avgTimeElapsed);

            if(RankedCounters.get(avgTimeElapsed)==null){
                RankedCounters.put(avgTimeElapsed,new ConcurrentSkipListSet<>());
            }
            RankedCounters.get(avgTimeElapsed).add(key);
            //CountersTimeRanked.add(noe);
        }
    }

    private void OrderTypesByInputEvents(Map<String,Double> frequencyOfInputEvents, Lettuce store) {
        for(String id : TimeToBuildMap.keySet()) {
            //For each Event Type ID that uses counters
            double lastTimetobuild = TimeToBuildMap.get(id);
            TimeToBuildMap.put(id,0.0);
            //String def = store.getEventTypeDefinition(id);
            String def = store.getEventTypeDefinition(id);
            long snumber = Long.parseLong(QueryAnalysis.getStateNumber(def));
            // Get the number of counters necessary to build its state
            for(String InputId : store.getEventTypeInputs(id)){
                // For each input of said Event Type Id
                if((1/frequencyOfInputEvents.get(InputId))*snumber> TimeToBuildMap.get(id)){
                    // if the time beetwen events of one input is greater than the last
                    TimeToBuildMap.put(id,(1/frequencyOfInputEvents.get(InputId))*snumber);
                }
            }
            TimeToBuildMap.put(id,(TimeToBuildMap.get(id)+lastTimetobuild)/2);
        }
    }


    private Map<String,Double> getFrequencyOfInputEventsPassed(Map<String, Receiver> receiverMap, Lettuce store){
        Map<String,Double> inputeventsPassed = new ConcurrentSkipListMap<>();
        for(String EtypeId : TimeToBuildMap.keySet()){
            for(String InputId : store.getEventTypeInputs(EtypeId)){
                long evtsPerPeriod = receiverMap.get(InputId).eventsPerPeriod();
                if(evtsPerPeriod!=0)
                    inputeventsPassed.put(InputId, (double) (evtsPerPeriod) /(double) (receiverMap.get(InputId).timeElapsed()));
                else
                    inputeventsPassed.put(InputId, (double) (1)/(double) (24*60*60*1000));
            }
        }

        return inputeventsPassed;
    }


}

/*
class ScoredStringD implements Comparable<ScoredStringD>{
    private String string;
    private final double score;

    public double getScore() {
        return score;
    }

    public ScoredStringD(String string, double score){
        this.string = string;
        this.score = score;
    }
    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public int compareTo(ScoredStringD other) {
        return (int) (score-other.score);
    }
}

 */







