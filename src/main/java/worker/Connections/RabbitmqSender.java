package worker.Connections;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import org.apache.avro.generic.GenericData.Record;

import worker.utils.Serializer;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class RabbitmqSender implements Sender{
    private final String exchange_name;
    private Connection connection;
    private Channel channel;
    private boolean stopMessages;
    private static final String rmode = System.getenv("RMODE");
    private int eventsLost;

    //private static Logger LOG = LoggerFactory.getLogger(RabbitmqSender.class);


    public RabbitmqSender(Connection c) {
        //LOG.info("Creating Rabbitmq sender connection");
        exchange_name = System.getenv("EXCHANGE");
        eventsLost = 0;
        stopMessages = false;
        try {
            connection = c;
            //connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(exchange_name, "topic");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqSender() {
        //LOG.info("Creating Rabbitmq sender connection");
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBITMQ_HOST"));
        exchange_name = System.getenv("EXCHANGE");
        factory.setUsername(System.getenv("RABBITMQ_USERNAME"));
        factory.setPassword(System.getenv("RABBITMQ_PASSWORD"));
        Connection connection;
        stopMessages = false;
        eventsLost = 0;
        try {
            //connection = c;
            connection = factory.newConnection();
            channel = connection.createChannel();
            channel.exchangeDeclare(exchange_name, "topic");
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }

    }

    public RabbitmqSender(Channel channel){
        exchange_name = "EXCHANGE";
        stopMessages = false;
        eventsLost = 0;
        try {
            channel.exchangeDeclare(exchange_name, "topic");
        } catch (IOException e) {
            e.printStackTrace();
        }

        this.channel = channel;

    }

    public void stopSending(){
        stopMessages = true;
    }

    public void restartSending(){
        stopMessages = false;
        eventsLost=0;
    }

    public int getEventsLost(){
        return eventsLost;
    }

    private void CloseConnection()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.close();
            connection.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }
    private void CloseChannel()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");
        try {
            channel.close();
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private void CloseQueue()  {
        //if(channel.isOpen()) LOG.info("Channel marked for closing\n");

    }




    public void close(String type){
        if(type.equals("channel")) CloseQueue();
        if(type.equals("conn")) CloseChannel();
        if(type.equals("")) CloseConnection();
    }




    /*public void CloseConnection() {

        if(rmode.equals("channel")) {
            try {
                channel.close();
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }
        }
        else {
            try {
                channel.close();
                connection.close();
            } catch (IOException | TimeoutException e) {
                e.printStackTrace();
            }
        }

    }

     */

    public void Publish(String routingKey,Record event)  {
        //System.out.print(event.getSchema().getName()+" --: "+"\n");
        if(!stopMessages) {
            //LOG.info("sending message");
            send(routingKey, event);
        }
        else
            eventsLost++;

    }


    private void send(String routingKey,Record event)  {

        try {
            channel.basicPublish(exchange_name, routingKey, null, Serializer.AvroSerialize(event));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }



}