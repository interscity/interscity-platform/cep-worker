package worker.ClusterHandler;

import worker.DatabaseAccess.Lettuce;

public interface ClusterHandler {

    int instantiateNeoWorker(Lettuce store);

    void StopAndRemoveWorkerFromCluster(String WorkerId, Lettuce store);
}
